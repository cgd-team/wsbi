module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    //grunt-beep
    //https://www.npmjs.org/package/grunt-beep
    // It beeps
    beep: {},

    //grunt-autoprefixer
    //
    //Goes to CanIUse and prefixes our CSS properties, then posts its changelog to the local _notes directory for human inspection
    autoprefixer: {
      options: {
        browsers: ['last 2 versions'],
        map: false,
        diff: '_notes/autoprefixer.diff.css'
      },
      files: {
        expand: true, // Enable dynamic expansion.
        cwd: '_scratch/', // Src matches are relative to this path.
        src: ['**/*.css'], // Actual pattern(s) to match.
        dest: '_scratch/', // Destination path prefix.
        ext: '.css', // Dest filepaths will have this extension.
        extDot: 'first', // Extensions in filenames begin after the first dot
      }
    },
    //grunt-contrib-sass
    //https://github.com/gruntjs/grunt-contrib-sass
    //compiles SCSS
    sass: {
      prod: {
        options: {
          style: 'expanded'
        },
        files: {
          '_scratch/<%= pkg.name %>.css': '_scss/<%= pkg.name %>.scss'
        }
      }
    },
    //grunt-contrib-less
    //https://github.com/gruntjs/grunt-contrib-less
    //compiles LESS
    less: {
      compileCore: {
        options: {
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: '<%= pkg.name %>.css.map',
          sourceMapFilename: '<%= pkg.root %>/css/<%= pkg.name %>.css.map'
        },
        src: '_less/bootstrap.less',
        dest: '_scratch/<%= pkg.name %>.css'
      },
      compileTheme: {
        options: {
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: '<%= pkg.name %>-theme.css.map',
          sourceMapFilename: '<%= pkg.root %>/css/<%= pkg.name %>-theme.css.map'
        },
        src: '_less/theme.less',
        dest: '_scratch/<%= pkg.name %>-theme.css'
      }
    },

    //grunt-csscomb
    //https://www.npmjs.org/package/grunt-csscomb
    //It organizes CSS properties based on a specified order
    csscomb: {
      options: {
        config: '.csscomb.json'
      },
      files: {
        expand: true, // Enable dynamic expansion.
        cwd: '_scratch/', // Src matches are relative to this path.
        src: ['**/*.css'], // Actual pattern(s) to match.
        dest: '_scratch/', // Destination path prefix.
        ext: '.css', // Dest filepaths will have this extension.
        extDot: 'first', // Extensions in filenames begin after the first dot
      }
    },
    //grunt-contrib-uglify
    //https://github.com/gruntjs/grunt-contrib-uglify
    //It processess and minifies JS
    uglify: {
      options: {
        mangle: true, //shortens and replaces variables names
        sourceMap: true,
        sourceMapIncludeSources: true
      },
      files: {
        expand: true, // Enable dynamic expansion.
        cwd: '_js/', // Src matches are relative to this path.
        src: ['**/*.js'], // Actual pattern(s) to match.
        dest: '<%= pkg.root %>/js/', // Destination path prefix.
        ext: '.min.js', // Dest filepaths will have this extension.
        extDot: 'last', // Extensions in filenames begin after the first dot
      }
    },
    //grunt-contrib-cssmin
    //https://npmjs.org/package/grunt-contrib-cssmin
    //It minifies CSS
    cssmin: {
      options: {
        banner: '/*! <%= pkg.name %> CSS version <%= pkg.version %> by Cooper Graphic Design */',
        keepSpecialComments: 0,
        report: 'min',
        noAdvanced: true
      },
      files: {
        expand: true, // Enable dynamic expansion.
        cwd: '_scratch/', // Src matches are relative to this path.
        src: ['**/*.css'], // Actual pattern(s) to match.
        dest: '<%= pkg.root %>/css/', // Destination path prefix.
        ext: '.min.css', // Dest filepaths will have this extension.
        extDot: 'last', // Extensions in filenames begin after the first dot
      }
    },
    //grunt-contrib-htmlmin
    //Google it
    //minifies HTML files and dumps them into the templates directory
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true,
          caseSensitive: true,
          minifyJS: true
        },
        files: [{
          expand: true, // Enable dynamic expansion.
          cwd: '_html/templates', // Src matches are relative to this path.
          src: ['**/*.html'], // Actual pattern(s) to match.
          dest: '<%= pkg.templates %>/', // Destination path prefix.
          ext: '.html', // Dest filepaths will have this extension.
          extDot: 'first' // Extensions in filenames begin after the first dot
        }]
      }
    },
    //grunt-ftp-deploy
    //Google it
    //Deploys to FTP server using credentials in a local key file
    'ftp-deploy': {
      build: {
        auth: {
          host: 'thefunds.nextmp.net',
          port: 21,
          authKey: 'key1'
        },
        src: '<%= pkg.root %>',
        dest: 'thefundsdp.org/<%= pkg.root %>',
        exclusions: ['/**/.DS_Store', '/**/Thumbs.db', '/**/*.bak', '/**/jpgtmp.jpg']
      }
    },
    //grunt-img
    //https://www.npmjs.com/package/grunt-img
    //optimizes images
    img: {
      // using only dirs with output path 
      all: {
        src: ['_images/*.png', '_images/*.jpg'],
        dest: '<%= pkg.root %>/img'
      }
    },

    //grunt-contrib-watch
    //https://github.com/gruntjs/grunt-contrib-watch
    //It fires tasks when files change
    watch: {
      scripts: {
        //path to source scripts
        files: ['_js/*.js'],
        tasks: ['uglify', 'beep']
      },
      styles: {
        //path to source styles
        files: ['_scss/*.scss'],
        tasks: ['dist-css', 'beep']
      }
    },
    //grunt-browser-sync
    //http://www.browsersync.io/docs/grunt/
    //A server that syncs inputs across connected devices
    browserSync: {
      dev: {
        bsFiles: {
          src: '<%= pkg.root %>/css/*.css'
        },
        options: {
          proxy: "localhost:8888"
        }
      }
    }
  });

  // Load the plugins that provide tasks.
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-beep');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-csscomb');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-ftp-deploy');
  grunt.loadNpmTasks('grunt-img');

  // Register tasks
  // This is what happens when you only type 'grunt'
  grunt.registerTask('default', ['beep']);
  // grunt.registerTask('dev-styles', ['watch:styles']);
  // grunt.registerTask('dev-scripts', ['watch:scripts']);

  // distribution tasks
  grunt.registerTask('dist-html', ['htmlmin', 'beep:1']);
  grunt.registerTask('dist-js', ['uglify', 'beep:1']);
  grunt.registerTask('dist-css', ['less', 'autoprefixer', 'csscomb', 'cssmin', 'beep:1']);
  grunt.registerTask('dist-img', ['img', 'beep:1']);
  grunt.registerTask('dist-code', ['dist-html', 'dist-js', 'dist-css']);
  grunt.registerTask('dist', ['dist-code', 'dist-img']);

  //deployment tasks
  grunt.registerTask('dist-deploy', ['dist', 'ftp-deploy']);
  grunt.registerTask('deploy', ['ftp-deploy']);

  //testing tasks
  //execute node domserver in pkg.root, then...
  grunt.registerTask('test', ['browserSync']);
};
