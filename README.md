# README

A repository for WSBI's revised website.

## Installing the Developer Environment

This project is built using [Grunt.js](http://gruntjs.com/), which requires [Node.js](http://nodejs.org/). We use [Sourcetree](http://www.sourcetreeapp.com/) to manage the [GIT](http://git-scm.com/) repository, but it's not required. If you're looking for a solid text editor, we recommend [SublimeText 3](http://www.sublimetext.com/).

1. Pull down this repository to a local directory. (If you're unfamiliar with GIT, hit the "Clone In Sourcetree" button).
2. Install [Node.js](http://nodejs.org/) and make sure it's accessible from your system PATH by using the command `node -v`.
3. Install [Grunt.js](http://gruntjs.com/) globally using the command line with `npm install grunt -g`.
4. Navigate to the root of the repository using the command line with `cd path\to\root`.
5. From the root of the repository, install the developer environment using the command line with `npm install`.
6. Create a `.ftppass` file in the root of the repository with your FTP credentials using proper JSON syntax:

```
{
  "key1": {
    "username": "ftp@coopergraphicdesign.com",
    "password": "ENTER_YOUR_PASSWORD_HERE"
  }
}
```

## Development Process

Be sure to set the `root` variable in `package.json` to the environment you're working on. The three environments are:

- Production: `html/`
- Development: `html/dev`
- Preview: `html/preview`

Be sure to set the `templates` variable in `package.json` to the environment you're working on. The three environments are:

- Production: `html/system/expressionengine/templates`
- Development: `html/dev/system/expressionengine/templates`
- Preview: `html/preview`

HTML Templates are located in `_html`. We distribute them to the environments using the `grunt dist-html` command. HTML files are linted and minified using [htmlmin](https://github.com/gruntjs/grunt-contrib-htmlmin) during this process.

[LESS](http://lesscss.org/) stylesheets are located in the `_less` directory. We distribute them to the environments using the `grunt dist-css` command. LESS files are [compiled into CSS](https://github.com/gruntjs/grunt-contrib-less), passed through [autoprefixer](https://github.com/nDmitry/grunt-autoprefixer), sorted using [csscomb](https://github.com/csscomb/grunt-csscomb), and minified using [cssmin](https://github.com/gruntjs/grunt-contrib-cssmin) during this process. The lastest compiled human-readable CSS file can be found in your local `_scratch` directory. The changes made by Autoprefixer can be found in your local `_notes` directory.

Javascript files are located in `_js`. We distribute them to the environments using the `grunt dist-js` command. JS files are minified using [uglify](https://github.com/gruntjs/grunt-contrib-uglify) during this process.

Image files are located in `_images` and are optimized with [imagemin](https://github.com/gruntjs/grunt-contrib-imagemin). We distribute them to the environments using the `grunt dist-img` command.

You can fire all of the distribution tasks at once using `grunt dist`, or just all the code with `grunt dist-code`.

**Never make changes to files in the `html` directory, as those changes may be overwritten the next time `grunt dist-html`, `grunt dist-css`, `grunt dist-js`, or `grunt dist` are called.**

*Remember to bump version numbers in `package.json` and to add yourself to `humans.txt`!*

## Testing Process

You can save yourself some agrivation by testing across all devices at once using [BrowserSync](http://www.browsersync.io/) and a node server.

1. Place domserver.js into the `html/preview` directory.
1. Install BrowserSync globally using the command line `npm install grunt-browser-sync -g`.
1. Run `node domserver` from within the `html/preview` directory.
1. Run `grunt test` within the project's root directory and navigate your testing devices to the IP addresses provided.

> Note: The BrowserSync control panel is located on port 3001. It's useful.

## Deployment Process

Deploy your current environment to the server from the command line using `grunt deploy` and remember to tag the current environment branch with the new version number.

## Help

Dom Bonanni can help. [Bother him](mailto:dombonanni@gmail.com).